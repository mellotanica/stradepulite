package main

import (
	"bufio"
	"errors"
	"fmt"
	"log"
	"os"
	"os/user"
	"strconv"
	"strings"
	"time"

	"github.com/go-telegram-bot-api/telegram-bot-api"
	"gopkg.in/ini.v1"
)

const (
	configPathFromHome = "~/.stradepulitebotrc"
	controlStop        = 0
	controlStart       = 1
	controlQuit        = 3
)

func botStart(resp *tgbotapi.MessageConfig) (*tgbotapi.MessageConfig, error) {
	resp.Text = "Sono il bot che ti aiuta con la pulizia strade di Bologa.\n" +
		"I comandi che mi puoi dare sono:\n" +
		"\t/strada <nome strada>: ti dico i giorni in cui fanno la pulizia nelle strade che si chiamano <nome strada>\n" +
		"\t/oggi: ti dico quali strade sono in lavaggio oggi\n" +
		"\t/settore <numero settore>: ti dico quali strade sono in lavaggio oggi in questo settore\n" +
		"\t/help: ti ripeto questo messaggio\n" +
		"\nOppure puoi mandarmi la tua posizione e ti dico quando c'e` il lavaggio li`\n" +
		"\nIo sono un bot open source, mi puoi trovare qui: https://gitlab.com/mellotanica/stradepulite"
	return resp, nil
}

func botStrada(query string, resp *tgbotapi.MessageConfig, db stradeDb) (*tgbotapi.MessageConfig, error) {
	if len(query) <= 0 {
		resp.Text = "mi devi dire una strada pero`..."
		return resp, nil
	}

	strade := db.QueryName(query)

	if len(strade) == 0 {
		resp.Text = "non ho trovato nessuna strada che si chiama cosi`, prova a semplificare la richiesta"
		return resp, nil
	}

	resp.Text = "il lavaggio verra` effettuato:\n"
	for _, s := range strade {
		resp.Text += s.String() + "\n"
	}
	return resp, nil
}

func botLocation(lat, lon float64, resp *tgbotapi.MessageConfig, db stradeDb) (*tgbotapi.MessageConfig, error) {
	loc, err := reverseGeocode(lat, lon, nil, nil, nil, nil)
	if err != nil {
		return resp, err
	}

	strade := db.QueryName(loc.Address.Road)

	resp.Text = "ti trovi qui: " + loc.Address.Road + "\n"

	if len(strade) == 0 {
		resp.Text += "non ho trovato nessuna strada che si chiama cosi`"
	} else {
		resp.Text += "il lavaggio verra` effettuato:\n"
		for _, s := range strade {
			resp.Text += s.String() + "\n"
		}
	}

	return resp, nil
}

func botOggi(resp *tgbotapi.MessageConfig, db stradeDb) (*tgbotapi.MessageConfig, error) {
	strade := db.QueryDay(nil)

	if len(strade) <= 0 {
		resp.Text = "oggi non si effettua il lavaggio"
	} else {
		resp.Text = "oggi il lavaggio si effettua in:\n"
		for _, s := range strade {
			resp.Text += s.Nome
			if len(s.Descrizione) > 0 {
				resp.Text += " (" + s.Descrizione + ")"
			}
			resp.Text += "\n"
		}
	}

	return resp, nil
}

func botZona(zona string, resp *tgbotapi.MessageConfig, db stradeDb) (*tgbotapi.MessageConfig, error) {
	z, err := strconv.Atoi(zona)
	if err != nil {
		resp.Text = "non conosco la zona " + zona + ", prova con 1/2/3/4/5"
		return resp, nil
	}
	strade := db.QueryDayZone(nil, z)

	if len(strade) <= 0 {
		resp.Text = "oggi non si effettua il lavaggio in questo settore"
	} else {
		resp.Text = fmt.Sprintf("nel settore %d oggi il lavaggio si effettua in:\n", z)
		for _, s := range strade {
			resp.Text += s.Nome
			if len(s.Descrizione) > 0 {
				resp.Text += " (" + s.Descrizione + ")"
			}
			resp.Text += "\n"
		}
	}

	return resp, nil
}

func handleUpdate(bot *tgbotapi.BotAPI, update tgbotapi.Update, db stradeDb) {
	err := errors.New("method not implemented")
	var resp *tgbotapi.MessageConfig

	if update.Message.IsCommand() {
		rsp := tgbotapi.NewMessage(update.Message.Chat.ID, "")
		switch update.Message.Command() {
		case "help":
			resp, err = botStart(&rsp)
		case "start":
			resp, err = botStart(&rsp)
		case "strada":
			log.Printf("Strada request from %s (%d): %s", update.Message.Chat.FirstName, update.Message.Chat.ID, update.Message.CommandArguments())
			resp, err = botStrada(update.Message.CommandArguments(), &rsp, db)
		case "oggi":
			resp, err = botOggi(&rsp, db)
		case "settore":
			resp, err = botZona(update.Message.CommandArguments(), &rsp, db)
		}
	}

	if update.Message.Location != nil {
		rsp := tgbotapi.NewMessage(update.Message.Chat.ID, "")
		log.Printf("Location request from %s (%d): lat %f, lon %f", update.Message.Chat.FirstName, update.Message.Chat.ID, update.Message.Location.Latitude, update.Message.Location.Longitude)
		resp, err = botLocation(update.Message.Location.Latitude, update.Message.Location.Longitude, &rsp, db)
	}

	if err == nil {
		_, err = bot.Send(resp)
		if err != nil {
			log.Printf("BOT: ERROR sending response to '%s': %s", update.Message.Text, err.Error())
		}
	}
}

func getToken() (tok string, err error) {
	path := configPathFromHome
	if path[0] == '~' {
		var u *user.User
		pathstart := 1

		if path[1] == '/' {
			u, err = user.Current()
		} else {
			pathstart = strings.Index(path, "/")
			u, err = user.Lookup(path[1:pathstart])
		}
		if err != nil {
			return
		}

		path = u.HomeDir + path[pathstart:]
	}
	if _, err = os.Stat(path); os.IsNotExist(err) {
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Enter telegram api key: ")
		tok, err = reader.ReadString('\n')
		if err != nil {
			return
		}

		cfg := ini.Empty()
		var s *ini.Section
		s, err = cfg.NewSection("main")
		if err != nil {
			return
		}

		_, err = s.NewKey("telegram_token", tok)
		if err != nil {
			return
		}

		err = cfg.SaveTo(path)
		if err != nil {
			return
		}
	} else {
		var cfg *ini.File
		cfg, err = ini.Load(path)
		if err != nil {
			return
		}

		tok = cfg.Section("main").Key("telegram_token").String()
	}
	return
}

func runbot(dbch chan stradeDb) error {
	tok, err := getToken()
	if err != nil {
		return err
	}

	bot, err := tgbotapi.NewBotAPI(tok)
	if err != nil {
		return err
	}

	log.Printf("starting bot with account: %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	// wait for updates and clear them to avoid handling
	// a large backlog of old messages
	time.Sleep(time.Millisecond * 500)
	updates.Clear()

	var db stradeDb

	for {
		select {
		case d := <-dbch:
			log.Println("BOT: received new db, replacing...")
			db = d
		case u := <-updates:
			if u.Message == nil {
				continue
			}

			handleUpdate(bot, u, db)
		}
	}
}
