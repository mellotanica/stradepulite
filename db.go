package main

import (
	"encoding/json"
	"io/ioutil"
	"strconv"
	"strings"
	"time"
)

type stradeDb struct {
	Strade []strada
	byName map[string][]*strada
	byDate map[int][]*strada
	zone   map[int][]*strada
}

func loadDb(filename string, settoriFile string) (db stradeDb, err error) {
	var f []byte
	f, err = ioutil.ReadFile(filename)
	if err != nil {
		return
	}

	err = json.Unmarshal(f, &(db.Strade))
	if err == nil {
		err = db.Init(settoriFile)
	}

	return
}

func stripParticle(str string, part string) string {
	res := strings.Replace(str, " "+part+" ", "", -1)
	if len(str) > len(part) && strings.Compare(res[:len(part)+1], part+" ") == 0 {
		res = res[len(part)+1:]
	}
	return res
}

func simplifyName(name string) string {
	res := strings.ToLower(name)

	ind := strings.Index(res, "(")
	if ind > 0 {
		res = res[:ind]
	}

	res = stripParticle(res, "di")
	res = stripParticle(res, "del")
	res = stripParticle(res, "della")
	res = stripParticle(res, "delle")
	res = stripParticle(res, "dello")
	res = stripParticle(res, "dei")
	res = stripParticle(res, "degli")
	res = stripParticle(res, "de'")
	res = stripParticle(res, "de'")

	res = strings.Replace(res, "dell'", "", -1)
	res = strings.Replace(res, "dell'", "", -1)

	res = strings.Replace(res, "largo ", "", -1)
	res = strings.Replace(res, "mura ", "", -1)
	res = strings.Replace(res, "piazza ", "", -1)
	res = strings.Replace(res, "piazzale ", "", -1)
	res = strings.Replace(res, "piazzetta ", "", -1)
	res = strings.Replace(res, "via ", "", -1)
	res = strings.Replace(res, "viale ", "", -1)
	res = strings.Replace(res, "vicolo ", "", -1)

	res = strings.Replace(res, "san ", "s. ", -1)
	res = strings.Replace(res, "santa ", "s. ", -1)
	res = strings.Replace(res, "santo ", "s. ", -1)
	res = strings.Replace(res, "sant'", "s. ", -1)

	res = strings.Replace(res, "s.", "s. ", -1)

	res = strings.Replace(res, "á", "a", -1)
	res = strings.Replace(res, "à", "a", -1)
	res = strings.Replace(res, "è", "e", -1)
	res = strings.Replace(res, "é", "e", -1)
	res = strings.Replace(res, "ì", "i", -1)
	res = strings.Replace(res, "í", "i", -1)
	res = strings.Replace(res, "ò", "o", -1)
	res = strings.Replace(res, "ó", "o", -1)
	res = strings.Replace(res, "ù", "u", -1)
	res = strings.Replace(res, "ú", "u", -1)

	res = strings.TrimSpace(res)
	if res[len(res)-1] == '\'' || res[len(res)-1] == '\'' {
		res = res[:len(res)-1]
	}

	return res
}

func (db *stradeDb) Init(settoriFile string) error {
	db.byName = make(map[string][]*strada)
	db.byDate = make(map[int][]*strada)
	for sid := range db.Strade {
		db.Strade[sid].NomeSpl = simplifyName(db.Strade[sid].Nome)
		if _, ok := db.byName[db.Strade[sid].NomeSpl]; !ok {
			db.byName[db.Strade[sid].NomeSpl] = make([]*strada, 0)
		}
		db.byName[db.Strade[sid].NomeSpl] = append(db.byName[db.Strade[sid].NomeSpl], &(db.Strade[sid]))

		day := db.Strade[sid].AbsDate()
		if _, ok := db.byDate[day]; !ok {
			db.byDate[day] = make([]*strada, 0)
		}
		db.byDate[day] = append(db.byDate[day], &(db.Strade[sid]))
	}
	if len(settoriFile) > 0 {
		db.zone = make(map[int][]*strada)

		f, err := ioutil.ReadFile(settoriFile)
		if err != nil {
			return err
		}

		var settoriJson map[string][]string
		err = json.Unmarshal(f, &settoriJson)
		if err != nil {
			return err
		}

		for si, list := range settoriJson {
			i, err := strconv.Atoi(si)
			if err != nil {
				return err
			}
			db.zone[i] = make([]*strada, 0)
			for _, s := range list {
				sm := db.QueryName(s)
				if sm != nil && len(sm) > 0 {
					db.zone[i] = append(db.zone[i], sm...)
					for _, street := range sm {
						found := false
						for _, zz := range street.Zone {
							if zz == i {
								found = true
							}
						}
						if !found {
							street.Zone = append(street.Zone, i)
						}
					}
				}
			}
		}
	}
	return nil
}

func (db *stradeDb) QueryName(query string) []*strada {
	subquery := strings.Split(simplifyName(query), " ")
	hits := make([][]*strada, len(subquery))
	for h := range hits {
		hits[h] = make([]*strada, 0)
	}

	for name, list := range db.byName {
		for qi, q := range subquery {
			if strings.Contains(name, q) {
				hits[qi] = append(hits[qi], list...)
			}
		}
	}

	var lower []*strada
	minh := len(db.Strade)

	for _, h := range hits {
		if len(h) < minh && len(h) > 0{
			lower = h
			minh = len(h)
		}
	}

	return lower
}

func (db *stradeDb) QueryDay(day *time.Time) []*strada {
	var res []*strada = nil
	var giorno time.Weekday
	var settimana int

	if day == nil {
		td := time.Now()
		if td.Hour() > 5 {
			td = td.Add(24 * time.Hour)
		}
		giorno, settimana = dayWeekFromDate(td)
	} else {
		giorno, settimana = dayWeekFromDate(*day)
	}

	d := AbsDate(giorno, settimana)

	var ok bool
	if res, ok = db.byDate[d]; !ok {
		res = make([]*strada, 0)
	}

	return res
}

func (db *stradeDb) QueryDayZone(day *time.Time, zone int) []*strada {
	list := db.QueryDay(day)
	ol := make([]*strada, 0)

	for _, s := range list {
		for _, z := range s.Zone {
			if z == zone {
				ol = append(ol, s)
			}
		}
	}

	return ol
}
