#!/bin/bash

OUTF="$(dirname "$BASH_SOURCE[0]")/../settori.json"

echo "{" > "$OUTF"
for f in *.poly; do
    sett="$(echo "$f" | sed -e 's/\.poly//')"
    echo -e "\t\"$sett\": [" >> "$OUTF"
    osmosis --read-pbf-fast bologna.osm.pbf file="bologna.osm.pbf" --bounding-polygon file="$sett.poly" --write-xml file="$sett.osm" > /dev/null
    osmfilter --keep="highway=residential =primary =secondary =tertiaty =unclassified =pedestrian" "$sett.osm" | grep name | sed -e 's/.*v=/\t\t/' -e 's/\/>$/,/' -e "s/&#39;/'/g" | sort | uniq >> "$OUTF"
    sed -i '$ s/.$//' "$OUTF"
    echo -e "\t]," >> "$OUTF"
    rm "$sett.osm"
done
sed -i '$ s/.$//' "$OUTF"
echo "}" >> "$OUTF"
