# StradePuliteBot

Bot di Telegram per rimanere aggiornati sulla pulizia strade a Bologna

Il bot risponde su Telegram all'username [@stradepulitebobot](https://t.me/stradepulitebobot)


Per richieste/consigli utilizzare la sezione [Issues](https://gitlab.com/mellotanica/stradepulite/issues)