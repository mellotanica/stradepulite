package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

const (
	reverseUrl = "https://nominatim.openstreetmap.org/reverse"
	searchUrl  = "https://nominatim.openstreetmap.org/search"
)

type revGeocodeRes struct {
	Latitude  string `json:"lat"`
	Longitude string `json:"lon"`
	Name      string `json:"display_name"`
	OsmType   string `json:"osm_type"`
	Address   struct {
		City        string
		County      string
		State       string
		Country     string
		CountryCode string `json:"country_code"`
		Road        string
	}
}

type searchRes []struct {
	Latitude  string `json:"lat"`
	Longitude string `json:"lon"`
	Name      string `json:"display_name"`
	Type      string
}

func reverseGeocode(latitude float64, longitude float64, osmType *string, zoom *int, addressdetail *int, email *string) (res revGeocodeRes, err error) {
	z := 18
	if zoom != nil {
		z = *zoom
	}
	rurl := fmt.Sprintf("%s?format=%s&lat=%f&lon=%f&zoom=%d", reverseUrl, url.QueryEscape("json"), latitude, longitude, z)
	if osmType != nil {
		rurl = fmt.Sprintf("%s&osm_type=%s", rurl, url.QueryEscape(*osmType))
	}

	if addressdetail != nil {
		rurl = fmt.Sprintf("%s&addressdetail=%d", rurl, *addressdetail)
	}

	if email != nil {
		rurl = fmt.Sprintf("%s&email=%s", rurl, url.QueryEscape(*email))
	}

	resp, err := http.Get(rurl)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	err = json.Unmarshal(data, &res)
	return
}

func search(query string, polygon *int, addressdetail *int, email *string) (res searchRes, err error) {
	rurl := fmt.Sprintf("%s?format=%s&q=%s", searchUrl, url.QueryEscape("json"), url.QueryEscape(query))
	if polygon != nil {
		rurl = fmt.Sprintf("%s&polygon=%d", rurl, *polygon)
	}

	if addressdetail != nil {
		rurl = fmt.Sprintf("%s&addressdetail=%d", rurl, *addressdetail)
	}

	if email != nil {
		rurl = fmt.Sprintf("%s&email=%s", rurl, url.QueryEscape(*email))
	}

	resp, err := http.Get(rurl)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	err = json.Unmarshal(data, &res)
	return
}
