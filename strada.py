import datetime

class strada:
    def __init__(self, id_strada, nome, quale, giorno, descrizione_alt):
        self.id_strada = id_strada

        self.nome = nome.strip()

        quale = quale.strip()
        self.quale_text = quale
        if 'il primo' in quale.lower() or 'la prima' in quale.lower():
            self.quale = 0
        elif 'il secondo' in quale.lower() or 'la seconda' in quale.lower():
            self.quale = 1
        elif 'il terzo' in quale.lower() or 'la terza' in quale.lower():
            self.quale = 2
        elif 'il quarto' in quale.lower() or 'la quarta' in quale.lower():
            self.quale = 3
        elif 'il quinto' in quale.lower() or 'la quinta' in quale.lower():
            self.quale = 4
        else:
            print("non capisco {}".format(quale), file=sys.stderr)
            self.quale = -1

        giorno = giorno.strip()
        self.giorno_text = giorno
        if 'domenica del mese' in giorno.lower():
            self.giorno = 0
        elif 'lunedì del mese' in giorno.lower():
            self.giorno = 1
        elif 'martedì del mese' in giorno.lower():
            self.giorno = 2
        elif 'mercoledì del mese' in giorno.lower():
            self.giorno = 3
        elif 'giovedì del mese' in giorno.lower():
            self.giorno = 4
        elif 'venerdì del mese' in giorno.lower():
            self.giorno = 5
        elif 'sabato del mese' in giorno.lower():
            self.giorno = 6
        else:
            print("non capisco il giorno {}".format(giorno), file=sys.stderr)
            self.giorno = -1

        self.descrizione_alt = descrizione_alt.strip()

        self.monthday = self.giorno + (self.quale * 7)

    def __hash__(self):
        return hash((self.id_strada,
                     self.nome,
                     self.quale,
                     self.giorno,
                     self.descrizione_alt,
                     self.quale_text,
                     self.giorno_text))

    def __eq__(self, other):
        return (self.id_strada == other.id_strada and
                self.nome == other.nome and
                self.quale == other.quale and
                self.giorno == other.giorno and
                self.descrizione_alt == other.descrizione_alt and
                self.quale_text == other.quale_text and
                self.giorno_text == other.giorno_text)

    def __str__(self):
        string = "{} ({}): {} ({}) {} ({})".format(self.nome, self.id_strada, self.quale_text, self.quale, self.giorno_text, self.giorno)
        if self.descrizione_alt != "":
            string += "\t -> \t{}".format(self.descrizione_alt)
        return string

    def dict(self):
        return {
            "id": self.id_strada,
            "nome": self.nome,
            "quale": self.quale,
            "giorno": self.giorno,
            "descrizione": self.descrizione_alt,
            "quale_text": self.quale_text,
            "giorno_text": self.giorno_text
        }

    @staticmethod
    def from_dict(d):
        s = strada(d['id'],
                   d['nome'],
                   d['quale_text'],
                   d['giorno_text'],
                   d['descrizione'])
        s.giorno = d['giorno']
        s.quale = d['quale']
        return s

    def next_real_day(self):
        fd = datetime.date(datetime.date.today().year, datetime.date.today().month, 1)
        nm = datetime.date(datetime.date.today().year, datetime.date.today().month + 1, 1)

        def actual_day_from_moth(first_day, giorno, quale):
            if first_day.weekday() < giorno:
                delta = datetime.timedelta(days=(quale * 7 + (giorno - first_day.weekday())))
            else:
                delta = datetime.timedelta(days=(quale * 7 + (7 - (first_day.weekday() - giorno))))
            return first_day + delta

        ad = actual_day_from_moth(fd, self.giorno, self.quale)
        if datetime.date.today() > ad:
            ad = actual_day_from_moth(nm, self.giorno, self.quale)

        return ad

