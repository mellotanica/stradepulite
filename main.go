package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"
)

func updateDb(ch chan stradeDb, sig chan os.Signal) {
	for {
		db, err := loadDb("strade_db.json", "settori.json")
		if err == nil {
			ch <- db
		} else {
			log.Printf("ERROR loading db: %s\n", err.Error())
		}
		<-sig
		log.Println("Received SIGUSR1, updating db")
	}
}

func main() {
	dbchan := make(chan stradeDb)
	sigchan := make(chan os.Signal)

	go updateDb(dbchan, sigchan)

	signal.Notify(sigchan, syscall.SIGUSR1)

	err := runbot(dbchan)
	if err != nil {
		log.Panic(err)
	}
}
