package main

import (
	"fmt"
	"time"
)

type strada struct {
	Id           string
	Nome         string
	NomeSpl      string
	Giorno       time.Weekday
	Settimana    int `json:"quale"`
	Descrizione  string
	GiornoTxt    string `json:"giorno_text"`
	SettimanaTxt string `json:"quale_text"`
	Zone         []int
}

func actualDayFromMonth(firstDay time.Time, giorno time.Weekday, settimana int) time.Time {
	var days int
	if firstDay.Weekday() < giorno {
		days = (settimana * 7) + int(giorno-firstDay.Weekday())
	} else {
		days = (settimana * 7) + (7 - int(firstDay.Weekday()-giorno))
	}
	delta, _ := time.ParseDuration(fmt.Sprintf("%dh", days*24))
	return firstDay.Add(delta)
}

func dayWeekFromDate(date time.Time) (day time.Weekday, week int) {
	week = 0
	curmonth := date.Month()
	day = date.Weekday()
	weekdelta := -1 * time.Hour * 24 * 7

	for {
		date = date.Add(weekdelta)
		if date.Month() != curmonth {
			break
		}
		week++
	}

	return
}

func (s *strada) NextRealDay() time.Time {
	now := time.Now()
	today := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
	firstOfMonth := time.Date(now.Year(), now.Month(), 1, 0, 0, 0, 0, now.Location())
	nextMonth := time.Date(now.Year(), now.Month()+1, 1, 0, 0, 0, 0, now.Location())

	actualDay := actualDayFromMonth(firstOfMonth, s.Giorno, s.Settimana)
	if today.After(actualDay) {
		return actualDayFromMonth(nextMonth, s.Giorno, s.Settimana)
	}
	return actualDay
}

func (s *strada) String() string {
	str := fmt.Sprintf("%s %s %s ", s.Nome, s.SettimanaTxt, s.GiornoTxt)
	nd := s.NextRealDay()
	str += fmt.Sprintf("(%d-%d-%d)", nd.Year(), nd.Month(), nd.Day())
	if len(s.Descrizione) > 0 {
		str += " -> " + s.Descrizione
	}
	return str
}

func AbsDate(d time.Weekday, s int) int {
	return int(d) + (s * 7)
}

func (s *strada) AbsDate() int {
	return AbsDate(s.Giorno, s.Settimana)
}
